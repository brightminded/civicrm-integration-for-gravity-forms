<?php
/**
Plugin Name: Gravity Forms for CiviCRM
Author: Bradley Taylor
Author URI: https://brightminded.com
Text Domain: gravityformscivi
**/

defined( 'ABSPATH' ) || die();

define('GF_CIVI_VERSION', 1);

define('GF_CIVI_PATH', plugin_dir_path(__FILE__));
define('GF_CIVI_URL', plugin_dir_url(__FILE__));

// If Gravity Forms is loaded, bootstrap the Add-On.
add_action( 'gform_loaded', array( 'GF_Civi_Bootstrap', 'load' ), 5 );

/**
 * Class GF_Civi_Bootstrap
 *
 * Handles the loading of the Add-On and registers with the Add-On framework.
 */
class GF_Civi_Bootstrap {

	/**
	 * If the Feed Add-On Framework exists, Add-On is loaded.
	 *
	 * @access public
	 * @static
	 */
	public static function load() {

		if ( ! method_exists( 'GFForms', 'include_feed_addon_framework' ) ) {
			return;
		}

		require_once( 'inc/class-civi-contact-repository.php' );
		require_once( 'inc/class-civi-created-log.php' );
		require_once( 'inc/class-civi-contact-ajax-handler.php' );
		require_once( 'inc/class-civi-entry-metabox.php' );

		require_once( 'feeds/class-feed-contact.php' );
        require_once( 'feeds/class-feed-email.php' );
		require_once( 'feeds/class-feed-activity.php' );

		GFAddOn::register( 'GF_Civi_Feed_Contact' );
        GFAddOn::register( 'GF_Civi_Feed_Activity' );
        GFAddOn::register( 'GF_Civi_Feed_Email' );

		add_filter( 'gform_entry_post_save', array( 'GF_Civi_Bootstrap', 'record_created_entities' ), 20, 2 );
	}

	public static function get_current_contact()
	{
		if ( is_user_logged_in() ) {
			$current_user = wp_get_current_user();
			$current_user = self::get_wp_civi_contact( $current_user->ID );
			return self::get_civi_contact( $current_user );
		}
		return false;
	}

	/**
	 * Get the CiviCRM Contact ID for a WordPress user ID.
	 *
	 * @since 0.1
	 *
	 * @param int $id The numeric WordPress user ID
	 * @return int $contact_id The numeric CiviCRM Contact ID
	 */
	public static function get_wp_civi_contact( $id )
	{

		$params = [
			'sequential' => 1,
			'uf_id' => $id,
			'domain_id' => CRM_Core_BAO_Domain::getDomain()->id,
		];

		try {
			$wp_civicrm_contact = civicrm_api3( 'UFMatch', 'getsingle', $params );
		} catch ( CiviCRM_API3_Exception $e ) {
			Civi::log()->debug( 'Unable to match contact for user with id ' . $id );
		}

		return $wp_civicrm_contact['contact_id'];

	}

	/**
	 * Get a CiviCRM Contact.
	 *
	 * @since 0.1
	 *
	 * @param int $cid The numeric Contact ID
	 * @param bool|array The Contact data array, or 0 if none retrieved
	 */
	public static function get_civi_contact( $cid )
	{

		if ( $cid != 0 ) {

			$params = [
				'sequential' => 1,
				'id' => $cid,
			];

			try {
				$fields = civicrm_api3( 'Contact', 'getsingle', $params );
			} catch ( CiviCRM_API3_Exception $e ) {

			}

			return $fields;

		} else {
			return 0;
		}
	}

	public static function get_contact_feeds($form_id)
	{
		$feeds = GFAPI::get_feeds([], $form_id, 'civicrm_contact', true);
		$return_arr = [
			[
				'label' => '',
				'value' => ''
			]
		];
		foreach ($feeds as $feed) {
			$return_arr[] = [
				'label' => $feed['meta']['feedName'],
				'value' => $feed['id']
			];
		}
		return $return_arr;
	}

	/**
	 * @param array $entry The Entry Object currently being processed.
	 * @param array $form  The Form Object currently being processed.
	 *
	 * @return array
	 */
	public static function record_created_entities($entry, $form)
	{
		gform_add_meta( $entry['id'], 'civi_successful_entities', json_encode(GF_Civi_Created_Log::get()->success), $form['id'] );
		gform_add_meta( $entry['id'], 'civi_failed_entities', json_encode(GF_Civi_Created_Log::get()->failure), $form['id'] );
	}
}
