<?php

defined( 'ABSPATH' ) || die();

GFForms::include_feed_addon_framework();

class GF_Civi_Feed_Activity extends GFFeedAddOn {

	/**
	 * Contains an instance of this class, if available.
	 *
	 * @since  1.0
	 * @access private
	 * @var    object $_instance If available, contains an instance of this class.
	 */
	private static $_instance = null;

	/**
	 * Defines the version of this class
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_version Contains the version
	 */
	protected $_version = GF_CIVI_VERSION;

	/**
	 * Defines the minimum Gravity Forms version required.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_min_gravityforms_version The minimum version required.
	 */
	protected $_min_gravityforms_version = '2.4.5';

	/**
	 * Defines the plugin slug.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_slug The slug used for this plugin.
	 */
	protected $_slug = 'civicrm_activity';

	/**
	 * Defines the main plugin file.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_path The path to the main plugin file, relative to the plugins folder.
	 */
	protected $_path = 'gravityformscivi/gravityformscivi.php';

	/**
	 * Defines the full path to this class file.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_full_path The full path.
	 */
	protected $_full_path = __FILE__;

	/**
	 * Defines the URL where this Add-On can be found.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string The URL of the Add-On.
	 */
	protected $_url = 'http://brightminded.com';

	/**
	 * Defines the title of this Add-On.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_title The title of the Add-On.
	 */
	protected $_title = 'CiviCRM extension for Gravity Forms';

	/**
	 * Defines the short title of the Add-On.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_short_title The short title.
	 */
	protected $_short_title = 'CiviCRM - Activity';

	/**
	 * Defines if Add-On should use Gravity Forms servers for update data.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    bool
	 */
	protected $_enable_rg_autoupgrade = false;

	/**
	 * Defines if Add-On should allow users to configure what order feeds are executed in.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    bool
	 */
	protected $_supports_feed_ordering = false;

	/**
	 * Defines the capability needed to access the Add-On settings page.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_capabilities_settings_page The capability needed to access the Add-On settings page.
	 */
	protected $_capabilities_settings_page = 'gravityforms_civi';

	/**
	 * Defines the capability needed to access the Add-On form settings page.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_capabilities_form_settings The capability needed to access the Add-On form settings page.
	 */
	protected $_capabilities_form_settings = 'gravityforms_civi';

	/**
	 * Defines the capability needed to uninstall the Add-On.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_capabilities_uninstall The capability needed to uninstall the Add-On.
	 */
	protected $_capabilities_uninstall = 'gravityforms_civi_uninstall';

	/**
	 * Defines the capabilities needed for the Advanced Post Creation Add-On
	 *
	 * @since  1.0
	 * @access protected
	 * @var    array $_capabilities The capabilities needed for the Add-On
	 */
	protected $_capabilities = array( 'gravityforms_civi', 'gravityforms_civi_uninstall' );

	/**
	 * The ID of the feed currently being processed.
	 *
	 * @since 1.0-beta-2.1
	 *
	 * @var bool|int
	 */
	private $_current_feed = false;

	public $activity_ignore_fields = [ 'target_contact_id', 'source_contact_id', 'assignee_contact_id', 'source_record_id', 'contact_id', 'details' ];

	/**
	 * Get instance of this class.
	 *
	 * @since  1.0
	 * @access public
	 * @static
	 *
	 * @return GF_Advanced_Post_Activity
	 */
	public static function get_instance() {

		if ( null === self::$_instance ) {
			self::$_instance = new self;
		}

		return self::$_instance;

	}

	/**
	 * Register needed hooks for Add-On.
	 *
	 * @since  1.0
	 * @access public
	 */
	public function pre_init() {

		parent::pre_init();

		add_filter( 'gform_export_form', array( $this, 'export_feeds_with_form' ) );
		add_action( 'gform_forms_post_import', array( $this, 'import_feeds_with_form' ) );

	}

	/**
	 * Register needed hooks for Add-On.
	 *
	 * @since  1.0
	 * @access public
	 */
	public function init() {

		parent::init();

		$this->add_delayed_payment_support(
			array(
				'option_label' => esc_html__( 'Create activity only when payment is received.', 'gravityformscivi' )
			)
		);

	}

		/**
	 * Register needed AJAX actions.
	 *
	 * @since  1.0
	 * @access public
	 */
	public function init_ajax() {

		parent::init_ajax();

		add_action( 'wp_ajax_gform_advancedpostcreation_taxonomy_search', array( $this, 'get_taxonomy_map_search_results' ) );
		add_action( 'wp_ajax_gform_advancedpostcreation_author_search', array( $this, 'get_post_author_search_results' ) );

	}

	/**
	 * Register needed styles.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @uses   GFAddOn::get_slug()
	 *
	 * @return array $styles
	 */
	public function styles() {

		$min = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG || isset( $_GET['gform_debug'] ) ? '' : '.min';

		$styles = array(
			array(
				'handle'  => 'selectwoo',
				'src'     => GF_CIVI_URL . "assets/selectwoo/selectwoo{$min}.css",
				'version' => $this->_version,
				'enqueue' => array(
					array(
						'admin_page' => array( 'form_settings' ),
					),
				),
			),
			array(
				'handle'  => 'selectwoo-gf-custom',
				'src'     => GF_CIVI_URL . "assets/css/selectwoo-gf-custom.css",
				'version' => $this->_version,
				'enqueue' => array(
					array(
						'admin_page' => array( 'form_settings' ),
					),
				),
			),
		);

		return array_merge( parent::styles(), $styles );

	}

	/**
	 * Register needed styles.
	 *
	 * @since  1.0
	 * @access public
	 *
	 *
	 * @return array $scripts
	 */
	public function scripts() {

		$min = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG || isset( $_GET['gform_debug'] ) ? '' : '.min';

		if (!$this->get_current_form()) {
			return array();
		}

		$scripts = array(
			array(
				'handle'  => 'selectwoo',
				'src'     => GF_CIVI_URL . "assets/selectwoo/selectwoo{$min}.js",
				'version' => $this->_version,
				'enqueue' => array(
					array(
						'admin_page' => array( 'form_settings' ),
					),
				),
			),
			array(
				'handle'  => 'gfcivi_contact_select',
				'src'     => GF_CIVI_URL . "assets/js/gfcivi-contact-select.js",
				'version' => $this->_version,
				'enqueue' => array(
					array(
						'admin_page' => array( 'form_settings' ),
					),
				),
				'strings' => array(
					'nonce' => wp_create_nonce('admin_get_civi_contact'),
					'form_id' => $this->get_current_form()['id']
				)
			),
		);

		return array_merge( parent::scripts(), $scripts );

	}


	// # FEED SETTINGS -------------------------------------------------------------------------------------------------

	/**
	 * Setup fields for feed settings.
	 *
	 * @return array
	 */
	public function feed_settings_fields() {

		// Build base fields array.
		$base_fields = array(
			array(
				'title'  => '',
				'fields' => array(
					array(
						'name'          => 'feedName',
						'label'         => esc_html__( 'Feed Name', 'gravityformscivi' ),
						'type'          => 'text',
						'required'      => true,
						'class'         => 'medium',
						'default_value' => $this->get_default_feed_name(),
						'tooltip'       => sprintf(
							'<h6>%s</h6>%s',
							esc_html__( 'Name', 'gravityformscivi' ),
							esc_html__( 'Enter a feed name to uniquely identify this setup.', 'gravityformscivi' )
						),
					),
					array(
						'name'  => 'action',
						'label' => esc_html__( 'Action', 'gravityformscivi' ),
						'type'  => 'hidden',
						'value' => 'post-create',
					),
				),
			),
		);

		// Build conditional logic fields.
		$conditional_fields = array(
			array(
				'fields' => array(
					array(
						'name'           => 'feedCondition',
						'type'           => 'feed_condition',
						'label'          => esc_html__( 'Conditional Logic', 'gravityformscivi' ),
						'checkbox_label' => esc_html__( 'Enable', 'gravityformscivi' ),
						'instructions'   => esc_html__( 'Create if', 'gravityformscivi' ),
						'tooltip'        => sprintf(
							'<h6>%s</h6>%s',
							esc_html__( 'Conditional Logic', 'gravityformscivi' ),
							esc_html__( 'When conditional logic is enabled, form submissions will only be created when the condition is met. When disabled, all form submissions will be created.', 'gravityformscivi' )
						),
					),
				),
			),
		);

		$create_activity_fields = $this->feed_settings_fields_create_activity();

		return array_merge( $base_fields, $create_activity_fields, $conditional_fields );

	}

	/**
	 * Setup fields for activity creation feed settings.
	 *
	 * @return array
	 */
	public function feed_settings_fields_create_activity() {

		$fields = array(
			'settings' => array(
				'id'     => 'contactSettings',
				'title'  => esc_html__( 'Activity Settings', 'gravityformscivi' ),
				'fields' => array(
					array(
						'name'          => 'linkTo',
						'label'         => esc_html__( 'Link To', 'gravityformscivi' ),
						'type'          => 'select',
						'required'      => false,
						'default_value' => '',
						'choices'       => GF_Civi_Bootstrap::get_contact_feeds($this->get_current_form()['id'])
					),
					array(
						'name'          => 'activityType',
						'label'         => esc_html__( 'Activity Type', 'gravityformscivi' ),
						'type'          => 'select',
						'required'      => false,
						'default_value' => '',
						'choices'       => $this->get_activity_types()
					),
					array(
						'name'          => 'activityStatus',
						'label'         => esc_html__( 'Activity Status', 'gravityformscivi' ),
						'type'          => 'select',
						'required'      => false,
						'default_value' => '',
						'choices'       => $this->get_activity_statuses()
					),
					array(
						'name'          => 'campaign',
						'label'         => esc_html__( 'Campaign', 'gravityformscivi' ),
						'type'          => 'select',
						'required'      => false,
						'default_value' => '',
						'choices'       => $this->get_crm_campaigns()
					),
					array(
						'name'          => 'targetContactID',
						'label'         => esc_html__( 'Target Contact ID', 'gravityformscivi' ),
						'type'          => 'select',
						'required'      => false,
						'default_value' => '',
						'class'         => 'gfcivi-crmcontacts-select',
						'choices'       => $this->get_select2_choice('targetContactID')
					),
					array(
						'name'          => 'assigneeContactID',
						'label'         => esc_html__( 'Assignee Contact ID', 'gravityformscivi' ),
						'type'          => 'select',
						'required'      => false,
						'default_value' => '',
						'class'         => 'gfcivi-crmcontacts-select',
						'choices'       => $this->get_select2_choice('assigneeContactID')
					),
				),
			),
			'content'  => array(
				'title'  => esc_html__( 'Activity Fields', 'gravityformscivi' ),
				'fields' => array(
					array(
						'name'          => 'details',
						'label'         => esc_html__( 'Details', 'gravityformsadvancedpostcreation' ),
						'type'          => 'textarea',
						'required'      => true,
						'class'         => 'medium merge-tag-support mt-position-right mt-hide_all_fields',
						'default_value' => '',
						'tooltip'       => sprintf(
							'<h6>%s</h6>%s',
							esc_html__( 'Post Content', 'gravityformsadvancedpostcreation' ),
							esc_html__( "Define the post's content. File upload field merge tags used within the post content will automatically have their files uploaded to the media library and associated with the post.", 'gravityformsadvancedpostcreation' )
						),

					),
					array(
						'name'        => 'activityFieldMap',
						'label'       => esc_html__( 'Field Mapping', 'gravityformscivi' ),
						'type'        => 'generic_map',
						'merge_tags'  => true,
						'key_field'   => array(
							'choices'     => $this->get_field_map(),
							'placeholder' => esc_html__( 'Custom Field Name', 'gravityformscivi' ),
							'title'       => esc_html__( 'Name', 'gravityformscivi' ),
						),
						'value_field' => array(
							'choices'      => 'form_fields',
							'custom_value' => true,
							'placeholder'  => esc_html__( 'Custom Field Value', 'gravityformscivi' ),
						)
					),
				)
			)
		);

		return $fields;

	}

	private function get_select2_choice($choice)
	{
		$feed = $this->get_current_feed();
		return GF_Civi_Contact_Ajax_Handler::get_select2_choice($choice, $feed);
	}

	private function get_activity_types()
	{
		$types = civicrm_api3('Activity', 'getoptions', [
			'sequential' => 1,
			'field' => 'activity_type_id',
		]);
		$return_arr = [
			[
				'label' => '',
				'value' => ''
			]
		];
		foreach ($types['values'] as $type) {
			$return_arr[] = [
				'label' => $type['value'],
				'value' => $type['key']
			];
		}
		return $return_arr;
	}

	private function get_activity_statuses()
	{
		$statues = civicrm_api3('Activity', 'getoptions', [
			'sequential' => 1,
			'field' => 'status_id',
		]);
		$return_arr = [
			[
				'label' => '',
				'value' => ''
			]
		];
		foreach ($statues['values'] as $status) {
			$return_arr[] = [
				'label' => $status['value'],
				'value' => $status['key']
			];
		}
		return $return_arr;
	}

	private function get_crm_campaigns()
	{
		$campaigns = civicrm_api3( 'Campaign', 'get', [
			'sequential' => 1,
			'is_active' => 1,
			'options' => [ 'limit' => 0 ],
		] );
		$return_arr = [
			[
				'label' => '',
				'value' => ''
			]
		];
		foreach ($campaigns['values'] as $key => $activity) {
			$return_arr[] = [
				'label' => $activity['value'],
				'value' => $activity['id']
			];
		}
		return $return_arr;
	}

	private function get_field_map()
	{
		$activity_fields_result = civicrm_api3( 'Activity', 'getfields', [
			'sequential' => 1,
		] );

		$activity_fields = [];
		foreach ( $activity_fields_result['values'] as $key => $value ) {
			if ( !in_array( $value['name'], $this->activity_ignore_fields ) ) {
				$activity_fields[] = [
					'label' => $value['title'],
					'value' => $value['name']
				];
			}
		}

		return $activity_fields;
	}

	// # FEED LIST -----------------------------------------------------------------------------------------------------

	/**
	 * Setup columns for feed list table.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @return array
	 */
	public function feed_list_columns() {

		return array(
			'feedName'   => esc_html__( 'Name', 'gravityformscivi' ),
		);

	}

	/**
	 * Enable feed duplication.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param string $id Feed ID requesting duplication.
	 *
	 * @return bool
	 */
	public function can_duplicate_feed( $id ) {

		return true;

	}

	/**
	 * Process the feed.
	 *
	 * @param  array $feed  The Feed object to be processed.
	 * @param  array $entry The Entry object currently being processed.
	 * @param  array $form  The Form object currently being processed.
	 *
	 * @return array
	 */
	public function process_feed( $feed, $entry, $form )
	{
		$custom_fields = $this->get_generic_map_fields( $feed, 'activityFieldMap', $form, $entry );

		$processed_fields = [];

		$processed_fields['source_contact_id'] = GF_Civi_Contact_Repository::get()->get_by_feed($feed['meta']['linkTo']);

		foreach ( $custom_fields as $meta_key => $meta_value ) {
			// Process merge tags.
			if ( GFCommon::has_merge_tag( $meta_value ) ) {
				$meta_value = GFCommon::replace_variables( $meta_value, $form, $entry );
			}

			$processed_fields[$meta_key] = $meta_value;
		}

		$processed_fields['details'] = GFCommon::replace_variables( $feed['meta']['details'], $form, $entry );

		$field_map = [
			'activityType' => 'activity_type_id',
			'activityStatus' => 'status_id',
			'campaign' => 'campaign_id',
		];
		foreach ($field_map as $key => $field) {
			if ($feed['meta'][$key]) {
				$processed_fields[$field] = $feed['meta'][$key];
			}
		}

		$contact_field_map = [
			'targetContactID' => 'target_contact_id',
			'assigneeContactID' => 'assignee_contact_id'
		];
		foreach ($contact_field_map as $key => $field) {
			if ($feed['meta'][$key]) {
				if (substr($feed['meta'][$key], 0, 5) === "feed_") {
					GF_Civi_Contact_Repository::get()->get_by_feed(substr($feed['meta'][$key], 5));
				} else {
					$processed_fields[$field] = $feed['meta'][$key];
				}
			}
		}

		try {
			civicrm_api3( 'Activity', 'create', $processed_fields );
		} catch ( CiviCRM_API3_Exception $e ) {
			echo $e->getMessage() . '<br><br><pre>' . $e->getTraceAsString() . '</pre>';
		}
	}

	// # IMPORT / EXPORT -----------------------------------------------------------------------------------------------

	/**
	 * Export Advanced Post Creation Add-On feeds when exporting form.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param array $form The current Form object being exported.
	 *
	 * @uses   GFAddOn::get_slug()
	 * @uses   GFFeedAddOn::get_feeds()
	 *
	 * @return array
	 */
	public function export_feeds_with_form( $form ) {

		// Get feeds for form.
		$feeds = $this->get_feeds( $form['id'] );

		// If form does not have a feeds property, create it.
		if ( ! isset( $form['feeds'] ) ) {
			$form['feeds'] = array();
		}

		// Add feeds to form object.
		$form['feeds'][ $this->get_slug() ] = $feeds;

		return $form;

	}

	/**
	 * Import Advanced Post Creation Add-On feeds when importing form.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param array $forms Imported Form objects.
	 *
	 * @uses   GFAPI::add_feed()
	 * @uses   GFAPI::get_form()
	 * @uses   GFAPI::update_form()
	 */
	public function import_feeds_with_form( $forms ) {

		// Loop through each form being imported.
		foreach ( $forms as $import_form ) {

			// Ensure the imported form is the latest. Compensates for a bug in Gravity Forms < 2.1.1.13
			$form = GFAPI::get_form( $import_form['id'] );

			// If the form does not have any Advanced Post Creation Add-On feeds, skip.
			if ( ! rgars( $form, 'feeds/' . $this->get_slug() ) ) {
				continue;
			}

			// Loop through feeds.
			foreach ( rgars( $form, 'feeds/' . $this->get_slug() ) as $feed ) {

				// Import feed.
				GFAPI::add_feed( $form['id'], $feed['meta'], $this->get_slug() );

			}

			// Remove feeds from Form object.
			unset( $form['feeds'][ $this->get_slug() ] );

			// Remove feeds property if empty.
			if ( empty( $form['feeds'] ) ) {
				unset( $form['feeds'] );
			}

			// Update form.
			GFAPI::update_form( $form );

		}

	}

}
