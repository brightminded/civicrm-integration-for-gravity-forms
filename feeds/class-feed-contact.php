<?php

defined( 'ABSPATH' ) || die();

GFForms::include_feed_addon_framework();

class GF_Civi_Feed_Contact extends GFFeedAddOn {

	/**
	 * Contains an instance of this class, if available.
	 *
	 * @since  1.0
	 * @access private
	 * @var    object $_instance If available, contains an instance of this class.
	 */
	private static $_instance = null;

	/**
	 * Defines the version of this class
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_version Contains the version
	 */
	protected $_version = GF_CIVI_VERSION;

	/**
	 * Defines the minimum Gravity Forms version required.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_min_gravityforms_version The minimum version required.
	 */
	protected $_min_gravityforms_version = '2.4.5';

	/**
	 * Defines the plugin slug.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_slug The slug used for this plugin.
	 */
	protected $_slug = 'civicrm_contact';

	/**
	 * Defines the main plugin file.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_path The path to the main plugin file, relative to the plugins folder.
	 */
	protected $_path = 'gravityformscivi/gravityformscivi.php';

	/**
	 * Defines the full path to this class file.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_full_path The full path.
	 */
	protected $_full_path = __FILE__;

	/**
	 * Defines the URL where this Add-On can be found.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string The URL of the Add-On.
	 */
	protected $_url = 'http://brightminded.com';

	/**
	 * Defines the title of this Add-On.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_title The title of the Add-On.
	 */
	protected $_title = 'CiviCRM extension for Gravity Forms';

	/**
	 * Defines the short title of the Add-On.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_short_title The short title.
	 */
	protected $_short_title = 'CiviCRM - Contacts';

	/**
	 * Defines if Add-On should use Gravity Forms servers for update data.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    bool
	 */
	protected $_enable_rg_autoupgrade = false;

	/**
	 * Defines if Add-On should allow users to configure what order feeds are executed in.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    bool
	 */
	protected $_supports_feed_ordering = false;

	/**
	 * Defines the capability needed to access the Add-On settings page.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_capabilities_settings_page The capability needed to access the Add-On settings page.
	 */
	protected $_capabilities_settings_page = 'gravityforms_civi';

	/**
	 * Defines the capability needed to access the Add-On form settings page.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_capabilities_form_settings The capability needed to access the Add-On form settings page.
	 */
	protected $_capabilities_form_settings = 'gravityforms_civi';

	/**
	 * Defines the capability needed to uninstall the Add-On.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_capabilities_uninstall The capability needed to uninstall the Add-On.
	 */
	protected $_capabilities_uninstall = 'gravityforms_civi_uninstall';

	/**
	 * Defines the capabilities needed for the Advanced Post Creation Add-On
	 *
	 * @since  1.0
	 * @access protected
	 * @var    array $_capabilities The capabilities needed for the Add-On
	 */
	protected $_capabilities = array( 'gravityforms_civi', 'gravityforms_civi_uninstall' );

	/**
	 * The ID of the feed currently being processed.
	 *
	 * @since 1.0-beta-2.1
	 *
	 * @var bool|int
	 */
	private $_current_feed = false;

	/**
	 * Get instance of this class.
	 *
	 * @since  1.0
	 * @access public
	 * @static
	 *
	 * @return GF_Civi_Feed_Contact
	 */
	public static function get_instance() {

		if ( null === self::$_instance ) {
			self::$_instance = new self;
		}

		return self::$_instance;

	}

	/**
	 * Register needed hooks for Add-On.
	 *
	 * @since  1.0
	 * @access public
	 */
	public function pre_init() {

		parent::pre_init();

		add_filter( 'gform_export_form', array( $this, 'export_feeds_with_form' ) );
		add_action( 'gform_forms_post_import', array( $this, 'import_feeds_with_form' ) );

	}

	/**
	 * Register needed hooks for Add-On.
	 *
	 * @since  1.0
	 * @access public
	 */
	public function init() {

		parent::init();

		$this->add_delayed_payment_support(
			array(
				'option_label' => esc_html__( 'Create contact only when payment is received.', 'gravityformscivi' )
			)
		);

	}


	// # FEED SETTINGS -------------------------------------------------------------------------------------------------

	/**
	 * Setup fields for feed settings.
	 *
	 * @return array
	 */
	public function feed_settings_fields() {

		// Build base fields array.
		$base_fields = array(
			array(
				'title'  => '',
				'fields' => array(
					array(
						'name'          => 'feedName',
						'label'         => esc_html__( 'Feed Name', 'gravityformscivi' ),
						'type'          => 'text',
						'required'      => true,
						'class'         => 'medium',
						'default_value' => $this->get_default_feed_name(),
						'tooltip'       => sprintf(
							'<h6>%s</h6>%s',
							esc_html__( 'Name', 'gravityformscivi' ),
							esc_html__( 'Enter a feed name to uniquely identify this setup.', 'gravityformscivi' )
						),
					)
				),
			),
		);

		// Build conditional logic fields.
		$conditional_fields = array(
			array(
				'fields' => array(
					array(
						'name'           => 'feedCondition',
						'type'           => 'feed_condition',
						'label'          => esc_html__( 'Conditional Logic', 'gravityformscivi' ),
						'checkbox_label' => esc_html__( 'Enable', 'gravityformscivi' ),
						'instructions'   => esc_html__( 'Create if', 'gravityformscivi' ),
						'tooltip'        => sprintf(
							'<h6>%s</h6>%s',
							esc_html__( 'Conditional Logic', 'gravityformscivi' ),
							esc_html__( 'When conditional logic is enabled, form submissions will only be created when the condition is met. When disabled, all form submissions will be created.', 'gravityformscivi' )
						),
					),
				),
			),
		);

		$create_post_fields = $this->feed_settings_fields_create_contact();

		return array_merge( $base_fields, $create_post_fields, $conditional_fields );

	}

	/**
	 * Setup fields for contact feed settings.
	 *
	 * @return array
	 */
	public function feed_settings_fields_create_contact() {

		$fields = array(
			'settings' => array(
				'id'     => 'contactSettings',
				'title'  => esc_html__( 'Contact Settings', 'gravityformscivi' ),
				'fields' => array(
					array(
						'name'          => 'creationMode',
						'label'         => esc_html__( 'Creation Mode', 'gravityformscivi' ),
						'type'          => 'select',
						'required'      => true,
						'default_value' => 'new',
						'choices'       => array(
							array(
								'label' => esc_html__( 'New Contact', 'gravityformscivi' ),
								'value' => 'new',
							),
							array(
								'label' => esc_html__( 'Update logged-in contact', 'gravityformscivi' ),
								'value' => 'updateLoggedIn',
							),
						),
					),
					array(
						'name'          => 'dedupeRule',
						'label'         => esc_html__( 'Dedupe Rule', 'gravityformscivi' ),
						'description' => 'You must only use a rule matching your contact type.',
						'type'          => 'select',
						'required'      => false,
						'default_value' => '',
						'choices'       => $this->get_dedupe_rules(),
						'tooltip'       => sprintf(
							'<h6>%s</h6>%s',
							esc_html__( 'Duplicate contact matching', 'gravityformscivi' ),
							esc_html__( 'Optionally select a Dedupe Rule. If a matching contact is found, that contact will be updated, and no new contact will be created.', 'gravityformscivi' )
						),
					),
					array(
						'name'          => 'dedupeEmail',
						'label'         => esc_html__( 'Dedupe Email', 'gravityformscivi' ),
						'description' => 'Used for deduping only',
						'type'          => 'field_select',
						'required'      => false,
						'default_value' => '',
						'dependency'    => 'dedupeRule',
						'tooltip'       => sprintf(
							'<h6>%s</h6>%s',
							esc_html__( 'Duplicate contact matching', 'gravityformscivi' ),
							esc_html__( 'If defined, this email address field will be used for deduping. If you need to save an email on a new contact, please create a `Civi - Email` feed', 'gravityformscivi' )
						),
					),
					array(
						'name'          => 'contactType',
						'label'         => esc_html__( 'Contact Type', 'gravityformscivi' ),
						'type'          => 'select',
						'required'      => true,
						'default_value' => 'individual',
						'choices'       => $this->get_contact_types()
					),
				),
			),
			'content'  => array(
				'title'  => esc_html__( 'Contact Details', 'gravityformscivi' ),
				'fields' => array(
					array(
						'name'        => 'contactFieldMap',
						'label'       => esc_html__( 'Field Mapping', 'gravityformscivi' ),
						'type'        => 'generic_map',
						'merge_tags'  => true,
						'key_field'   => array(
							'choices'     => $this->get_field_map(),
							'placeholder' => esc_html__( 'Custom Field Name', 'gravityformscivi' ),
							'title'       => esc_html__( 'Name', 'gravityformscivi' ),
						),
						'value_field' => array(
							'choices'      => 'form_fields',
							'custom_value' => true,
							'placeholder'  => esc_html__( 'Custom Field Value', 'gravityformscivi' ),
						)
					),
					array(
						'name'        => 'contactCustomFieldMap',
						'label'       => esc_html__( 'Custom Field Mapping', 'gravityformscivi' ),
						'description' => 'You must only use fields supported by your contact type. Valid values are not validated.',
						'type'        => 'generic_map',
						'merge_tags'  => true,
						'key_field'   => array(
							'choices'     => $this->get_custom_fields_map(),
							'placeholder' => esc_html__( 'Custom Field Name', 'gravityformscivi' ),
							'title'       => esc_html__( 'Name', 'gravityformscivi' ),
						),
						'value_field' => array(
							'choices'      => 'form_fields',
							'custom_value' => true,
							'placeholder'  => esc_html__( 'Custom Field Value', 'gravityformscivi' ),
						)
					),
				),
			)
		);

		return $fields;

	}

	public function get_field_map()
	{
		$custom_group = civicrm_api4('Contact', 'getFields', [
			'loadOptions' => ['name', 'label', 'description']
		]);

		$custom_fields = [];
		foreach ( $custom_group as $key => $value ) {
			if ($value['custom_field_id'] !== null) {
				continue;
			}

			$custom_fields[] = [
				'label' => $value['label'],
				'value' => $value['options'] ? $value['name'] . ':name' : $value['name']
			];
		}

		return $custom_fields;
	}

	public function get_dedupe_rules()
	{
		$dedupe_rules['Organization'] = CRM_Dedupe_BAO_RuleGroup::getByType( 'Organization' );
		$dedupe_rules['Individual'] = CRM_Dedupe_BAO_RuleGroup::getByType( 'Individual' );
		$dedupe_rules['Household'] = CRM_Dedupe_BAO_RuleGroup::getByType( 'Household' );

		$return_arr = [
			[
				'label' => '',
				'value' => ''
			]
		];
		foreach ($dedupe_rules as $type => $rules) {
			foreach ($rules as $key => $value) {
				$return_arr[] = [
					'label' => "$type - $value",
					'value' => $key
				];
			}
		}
		return $return_arr;
	}

	public function get_contact_types()
	{
		$contact_type_result = civicrm_api3( 'ContactType', 'get', [
			'sequential' => 0,
			'is_active' => 1,
			'parent_id' => [ 'IS NULL' => 1 ],
			'options' => [ 'limit' => 0 ],
		] );

		$contact_sub_type_result = civicrm_api3( 'ContactType', 'get', [
			'sequential' => 1,
			'parent_id' => [ 'IS NOT NULL' => 1 ],
			'is_active' => 1,
			'options' => [ 'limit' => 0 ],
		] );

		$contact_types = [];
		foreach ( $contact_type_result['values'] as $key => $value ) {
			$contact_type = [
				'label' => $value['label'],
				'value' => $value['name']
			];

			$subtypes = wp_list_filter($contact_sub_type_result['values'], [
				'parent_id' => $value['id']
			]);
			if ($subtypes) {
				$contact_type['choices'] = [];
				$contact_type['choices'][] = [
					'label' => $value['label'],
					'value' => $value['name']
				];
				foreach ($subtypes as $subtype) {
					$contact_type['choices'][] = [
						'label' => $subtype['label'],
						'value' => $value['name'] . '__subtype__' . $subtype['name']
					];
				}
			}
			$contact_types[] = $contact_type;
		}

		return $contact_types;
	}

	public function get_custom_fields_map()
	{
		$custom_group = civicrm_api4('Contact', 'getFields', [
			'loadOptions' => ['name', 'label', 'description']
		]);

		$custom_fields = [];
		foreach ( $custom_group as $key => $value ) {
			if ($value['custom_field_id'] === null) {
				continue;
			}

			$custom_fields[] = [
				'label' => $value['label'],
				'value' => $value['options'] ? $value['name'] . ':name' : $value['name']
			];
		}

		return $custom_fields;
	}

	// # FEED LIST -----------------------------------------------------------------------------------------------------

	/**
	 * Setup columns for feed list table.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @return array
	 */
	public function feed_list_columns() {

		return array(
			'feedName'   => esc_html__( 'Name', 'gravityformscivi' ),
		);

	}

	/**
	 * Enable feed duplication.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param string $id Feed ID requesting duplication.
	 *
	 * @return bool
	 */
	public function can_duplicate_feed( $id ) {

		return true;

	}

	/**
	 * Process the feed.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param  array $feed  The Feed object to be processed.
	 * @param  array $entry The Entry object currently being processed.
	 * @param  array $form  The Form object currently being processed.
	 *
	 * @uses   GFAddOn::log_debug()
	 * @uses   GF_Civi_Feed_Contact::create_post()
	 *
	 * @return array
	 */
	public function process_feed( $feed, $entry, $form )
	{
		$meta_fields = $this->get_generic_map_fields( $feed, 'contactFieldMap', $form, $entry );
		$custom_fields = $this->get_generic_map_fields( $feed, 'contactCustomFieldMap', $form, $entry );

		$processed_values = [];

		$contact_types = explode('__subtype__', $feed['meta']['contactType']);
		if (count($contact_types) === 2) {
			$processed_values['contact_type'] = $contact_types[0];
			$processed_values['contact_sub_type'] = $contact_types[1];
		} else {
			$processed_values['contact_type'] = $feed['meta']['contactType'];
		}

		foreach ( array_merge($meta_fields, $custom_fields) as $meta_key => $meta_value ) {
			// Process merge tags.
			if ( GFCommon::has_merge_tag( $meta_value ) ) {
				$meta_value = GFCommon::replace_variables( $meta_value, $form, $entry );
			}

			$processed_values[$meta_key] = $meta_value;
		}

		$current_contact = GF_Civi_Bootstrap::get_current_contact();
		if ($feed['meta']['creationMode'] === 'updateLoggedIn' && $current_contact) {
			$processed_values['id'] = $current_contact;
		} else if (!empty($feed['meta']['dedupeRule'])) {
			$dedupe_values = $processed_values;
			if ($feed['meta']['dedupeEmail'] && $entry[$feed['meta']['dedupeEmail']]) {
				$dedupe_values['email'] = $entry[$feed['meta']['dedupeEmail']];
			}
			$processed_values['id'] = $this->civi_contact_dedupe(
				$dedupe_values,
				$processed_values['contact_type'],
				$feed['meta']['dedupeRule']
			);
		}

		$creation_mode = $processed_values['id'] === 0 ? 'create' : 'update';

		try {
			$created = civicrm_api4('Contact', 'save', [
				'records' => [
					$processed_values
				],
				'checkPermissions' => FALSE,
			]);
			GF_Civi_Contact_Repository::get()->record($created->first()['id'], $feed['id']);

			GF_Civi_Created_Log::get()->record_success($created->first()['id'], false, [
				'entity' => 'Contact',
				'mode' => $creation_mode,
				'feed' => $feed['id']
			]);
		} catch ( Exception $e ) {
			GF_Civi_Created_Log::get()->record_failure([
				'entity' => 'Contact',
				'mode' => $creation_mode,
				'feed' => $feed['id'],
				'error' => $e->getMessage()
			]);
		}

        return $entry;
	}

	/**
	 * Dedupe CiviCRM Contact.
	 *
	 * @param array $contact Contact data
	 * @param string $contact_type Contact type
	 * @param int $dedupe_rule_id Dedupe Rule ID
	 * @return int $contact_id The contact id
	 */
	public function civi_contact_dedupe( $contact, $contact_type, $dedupe_rule_id ) {
		// Dupes params
		$dedupeParams = CRM_Dedupe_Finder::formatParams( $contact, $contact_type );
		$dedupeParams['check_permission'] = FALSE;

		// Check dupes
		$cids = CRM_Dedupe_Finder::dupesByParams( $dedupeParams, $contact_type, NULL, [], $dedupe_rule_id );
		$cids = array_reverse( $cids );

		return $cids ? array_pop( $cids ) : 0;
	}


	// # IMPORT / EXPORT -----------------------------------------------------------------------------------------------

	/**
	 * Export Advanced Post Creation Add-On feeds when exporting form.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param array $form The current Form object being exported.
	 *
	 * @uses   GFAddOn::get_slug()
	 * @uses   GFFeedAddOn::get_feeds()
	 *
	 * @return array
	 */
	public function export_feeds_with_form( $form ) {

		// Get feeds for form.
		$feeds = $this->get_feeds( $form['id'] );

		// If form does not have a feeds property, create it.
		if ( ! isset( $form['feeds'] ) ) {
			$form['feeds'] = array();
		}

		// Add feeds to form object.
		$form['feeds'][ $this->get_slug() ] = $feeds;

		return $form;

	}

	/**
	 * Import Advanced Post Creation Add-On feeds when importing form.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param array $forms Imported Form objects.
	 *
	 * @uses   GFAPI::add_feed()
	 * @uses   GFAPI::get_form()
	 * @uses   GFAPI::update_form()
	 */
	public function import_feeds_with_form( $forms ) {

		// Loop through each form being imported.
		foreach ( $forms as $import_form ) {

			// Ensure the imported form is the latest. Compensates for a bug in Gravity Forms < 2.1.1.13
			$form = GFAPI::get_form( $import_form['id'] );

			// If the form does not have any Advanced Post Creation Add-On feeds, skip.
			if ( ! rgars( $form, 'feeds/' . $this->get_slug() ) ) {
				continue;
			}

			// Loop through feeds.
			foreach ( rgars( $form, 'feeds/' . $this->get_slug() ) as $feed ) {

				// Import feed.
				GFAPI::add_feed( $form['id'], $feed['meta'], $this->get_slug() );

			}

			// Remove feeds from Form object.
			unset( $form['feeds'][ $this->get_slug() ] );

			// Remove feeds property if empty.
			if ( empty( $form['feeds'] ) ) {
				unset( $form['feeds'] );
			}

			// Update form.
			GFAPI::update_form( $form );

		}

	}

}
