<?php

defined( 'ABSPATH' ) || die();

GFForms::include_feed_addon_framework();

class GF_Civi_Feed_Email extends GFFeedAddOn {

	/**
	 * Contains an instance of this class, if available.
	 *
	 * @since  1.0
	 * @access private
	 * @var    object $_instance If available, contains an instance of this class.
	 */
	private static $_instance = null;

	/**
	 * Defines the version of this class
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_version Contains the version
	 */
	protected $_version = GF_CIVI_VERSION;

	/**
	 * Defines the minimum Gravity Forms version required.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_min_gravityforms_version The minimum version required.
	 */
	protected $_min_gravityforms_version = '2.4.5';

	/**
	 * Defines the plugin slug.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_slug The slug used for this plugin.
	 */
	protected $_slug = 'civicrm_email';

	/**
	 * Defines the main plugin file.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_path The path to the main plugin file, relative to the plugins folder.
	 */
	protected $_path = 'gravityformscivi/gravityformscivi.php';

	/**
	 * Defines the full path to this class file.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_full_path The full path.
	 */
	protected $_full_path = __FILE__;

	/**
	 * Defines the URL where this Add-On can be found.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string The URL of the Add-On.
	 */
	protected $_url = 'http://brightminded.com';

	/**
	 * Defines the title of this Add-On.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_title The title of the Add-On.
	 */
	protected $_title = 'CiviCRM extension for Gravity Forms';

	/**
	 * Defines the short title of the Add-On.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_short_title The short title.
	 */
	protected $_short_title = 'CiviCRM - Email';

	/**
	 * Defines if Add-On should use Gravity Forms servers for update data.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    bool
	 */
	protected $_enable_rg_autoupgrade = false;

	/**
	 * Defines if Add-On should allow users to configure what order feeds are executed in.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    bool
	 */
	protected $_supports_feed_ordering = false;

	/**
	 * Defines the capability needed to access the Add-On settings page.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_capabilities_settings_page The capability needed to access the Add-On settings page.
	 */
	protected $_capabilities_settings_page = 'gravityforms_civi';

	/**
	 * Defines the capability needed to access the Add-On form settings page.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_capabilities_form_settings The capability needed to access the Add-On form settings page.
	 */
	protected $_capabilities_form_settings = 'gravityforms_civi';

	/**
	 * Defines the capability needed to uninstall the Add-On.
	 *
	 * @since  1.0
	 * @access protected
	 * @var    string $_capabilities_uninstall The capability needed to uninstall the Add-On.
	 */
	protected $_capabilities_uninstall = 'gravityforms_civi_uninstall';

	/**
	 * Defines the capabilities needed for the Advanced Post Creation Add-On
	 *
	 * @since  1.0
	 * @access protected
	 * @var    array $_capabilities The capabilities needed for the Add-On
	 */
	protected $_capabilities = array( 'gravityforms_civi', 'gravityforms_civi_uninstall' );

	/**
	 * The ID of the feed currently being processed.
	 *
	 * @since 1.0-beta-2.1
	 *
	 * @var bool|int
	 */
	private $_current_feed = false;

	/**
	 * Get instance of this class.
	 *
	 * @since  1.0
	 * @access public
	 * @static
	 *
	 * @return GF_Civi_Feed_Contact
	 */
	public static function get_instance() {

		if ( null === self::$_instance ) {
			self::$_instance = new self;
		}

		return self::$_instance;

	}

	/**
	 * Register needed hooks for Add-On.
	 *
	 * @since  1.0
	 * @access public
	 */
	public function pre_init() {

		parent::pre_init();

		add_filter( 'gform_export_form', array( $this, 'export_feeds_with_form' ) );
		add_action( 'gform_forms_post_import', array( $this, 'import_feeds_with_form' ) );

	}

	/**
	 * Register needed hooks for Add-On.
	 *
	 * @since  1.0
	 * @access public
	 */
	public function init() {

		parent::init();
	}


	// # FEED SETTINGS -------------------------------------------------------------------------------------------------

	/**
	 * Setup fields for feed settings.
	 *
	 * @return array
	 */
	public function feed_settings_fields() {

		// Build base fields array.
		$base_fields = array(
			array(
				'title'  => '',
				'fields' => array(
					array(
						'name'          => 'feedName',
						'label'         => esc_html__( 'Feed Name', 'gravityformscivi' ),
						'type'          => 'text',
						'required'      => true,
						'class'         => 'medium',
						'default_value' => $this->get_default_feed_name(),
						'tooltip'       => sprintf(
							'<h6>%s</h6>%s',
							esc_html__( 'Name', 'gravityformscivi' ),
							esc_html__( 'Enter a feed name to uniquely identify this setup.', 'gravityformscivi' )
						),
					)
				),
			),
		);

		// Build conditional logic fields.
		$conditional_fields = array(
			array(
				'fields' => array(
					array(
						'name'           => 'feedCondition',
						'type'           => 'feed_condition',
						'label'          => esc_html__( 'Conditional Logic', 'gravityformscivi' ),
						'checkbox_label' => esc_html__( 'Enable', 'gravityformscivi' ),
						'instructions'   => esc_html__( 'Create if', 'gravityformscivi' ),
						'tooltip'        => sprintf(
							'<h6>%s</h6>%s',
							esc_html__( 'Conditional Logic', 'gravityformscivi' ),
							esc_html__( 'When conditional logic is enabled, form submissions will only be created when the condition is met. When disabled, all form submissions will be created.', 'gravityformscivi' )
						),
					),
				),
			),
		);

		$create_post_fields = $this->feed_settings_fields_create_email();

		return array_merge( $base_fields, $create_post_fields, $conditional_fields );

	}

	/**
	 * Setup fields for contact feed settings.
	 *
	 * @return array
	 */
	public function feed_settings_fields_create_email() {

		$fields = array(
			'settings' => array(
				'id'     => 'contactSettings',
				'title'  => esc_html__( 'Email Settings', 'gravityformscivi' ),
				'fields' => array(
					array(
						'name'          => 'linkTo',
						'label'         => esc_html__( 'Link To', 'gravityformscivi' ),
						'type'          => 'select',
						'required'      => true,
						'default_value' => '',
						'choices'       => GF_Civi_Bootstrap::get_contact_feeds($this->get_current_form()['id'])
					),
				)
			),
			'email'  => array(
				'title'  => esc_html__( 'Email Details', 'gravityformscivi' ),
				'fields' => array(
					array(
						'name'          => 'email',
						'label'         => esc_html__( 'Activity Type', 'gravityformscivi' ),
						'type'          => 'field_select',
						'required'      => true,
					),
					array(
						'name'          => 'locationTypeId',
						'label'         => esc_html__( 'Location', 'gravityformscivi' ),
						'type'          => 'select',
						'required'      => true,
						'default_value' => '',
						'choices'       => $this->get_email_locations()
					),
					array(
						'name'          => 'isPrimary',
						'label'         => esc_html__( 'Is Primary?', 'gravityformscivi' ),
						'type'          => 'checkbox',
						'choices'       => array(
							array(
								'label'         => esc_html__( 'Is Primary?', 'sometextdomain' ),
								'name'          => 'isPrimary',
								'default_value' => 1,
							)
						)
					),
					array(
						'name'          => 'isBilling',
						'label'         => esc_html__( 'Is Billing?', 'gravityformscivi' ),
						'type'          => 'checkbox',
						'choices'       => array(
							array(
								'label'         => esc_html__( 'Is Billing?', 'sometextdomain' ),
								'name'          => 'isBilling',
								'default_value' => 1,
							)
						)
					),
					array(
						'name'          => 'isHold',
						'isBulk',
						'label'         => esc_html__( 'Is on Hold?', 'gravityformscivi' ),
						'type'          => 'checkbox',
						'choices'       => array(
							array(
								'label'         => esc_html__( 'Is on Hold?', 'sometextdomain' ),
								'name'          => 'isHold',
								'isBulk',
								'default_value' => 1,
							)
						)
					),
					array(
						'name'          => 'isBulk',
						'label'         => esc_html__( 'Is Bulk?', 'gravityformscivi' ),
						'type'          => 'checkbox',
						'choices'       => array(
							array(
								'label'         => esc_html__( 'Is Bulk?', 'sometextdomain' ),
								'name'          => 'isBulk',
								'default_value' => 1,
							)
						)
					),
				),
			),
			'match'  => array(
				'title'  => esc_html__( 'Update emails by match', 'gravityformscivi' ),
				'fields' => array(
					array(
						'name'          => 'emailMatch',
						'label'         => esc_html__( 'Email address', 'gravityformscivi' ),
						'type'          => 'checkbox',
						'choices'       => array(
							array(
								'label'         => esc_html__( 'Match email address', 'sometextdomain' ),
								'name'          => 'emailMatch',
								'default_value' => 0,
							)
						)
					),
					array(
						'name'          => 'locationTypeIdMatch',
						'label'         => esc_html__( 'Location Type', 'gravityformscivi' ),
						'type'          => 'checkbox',
						'choices'       => array(
							array(
								'label'         => esc_html__( 'Match Location Type', 'sometextdomain' ),
								'name'          => 'locationTypeIdMatch',
								'default_value' => 0,
							)
						)
					),
					array(
						'name'          => 'isPrimaryMatch',
						'label'         => esc_html__( 'Is Primary?', 'gravityformscivi' ),
						'type'          => 'checkbox',
						'choices'       => array(
							array(
								'label'         => esc_html__( 'Match is Primary?', 'sometextdomain' ),
								'name'          => 'isPrimaryMatch',
								'default_value' => 0,
							)
						)
					),
					array(
						'name'          => 'isBillingMatch',
						'label'         => esc_html__( 'Is Billing?', 'gravityformscivi' ),
						'type'          => 'checkbox',
						'choices'       => array(
							array(
								'label'         => esc_html__( 'Match is Billing?', 'sometextdomain' ),
								'name'          => 'isBillingMatch',
								'default_value' => 0,
							)
						)
					),
					array(
						'name'          => 'isHoldMatch',
						'label'         => esc_html__( 'Is on Hold?', 'gravityformscivi' ),
						'type'          => 'checkbox',
						'choices'       => array(
							array(
								'label'         => esc_html__( 'Match is on Hold?', 'sometextdomain' ),
								'name'          => 'isHoldMatch',
								'default_value' => 0,
							)
						)
					),
					array(
						'name'          => 'isBulkMatch',
						'label'         => esc_html__( 'Is Bulk?', 'gravityformscivi' ),
						'type'          => 'checkbox',
						'choices'       => array(
							array(
								'label'         => esc_html__( 'Match is Bulk?', 'sometextdomain' ),
								'name'          => 'isBulkMatch',
								'default_value' => 0,
							)
						)
					),
				),
			)
		);

		return $fields;

	}

	public function get_email_locations()
	{
		$types = civicrm_api3( 'Email', 'getoptions', [
			'sequential' => 1,
			'field' => 'location_type_id',
		] );

		$return_arr = [
			[
				'label' => '',
				'value' => ''
			]
		];
		foreach ($types['values'] as $type) {
			$return_arr[] = [
				'label' => $type['value'],
				'value' => $type['key']
			];
		}
		return $return_arr;
	}

	// # FEED LIST -----------------------------------------------------------------------------------------------------

	/**
	 * Setup columns for feed list table.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @return array
	 */
	public function feed_list_columns() {

		return array(
			'feedName'   => esc_html__( 'Name', 'gravityformscivi' ),
		);

	}

	/**
	 * Enable feed duplication.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param string $id Feed ID requesting duplication.
	 *
	 * @return bool
	 */
	public function can_duplicate_feed( $id ) {

		return true;

	}

	/**
	 * Process the feed.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param  array $feed  The Feed object to be processed.
	 * @param  array $entry The Entry object currently being processed.
	 * @param  array $form  The Form object currently being processed.
	 *
	 * @uses   GFAddOn::log_debug()
	 * @uses   GF_Civi_Feed_Contact::create_post()
	 *
	 * @return array
	 */
	public function process_feed( $feed, $entry, $form )
	{
		$processed_fields = [];

		$contact_id = GF_Civi_Contact_Repository::get()->get_by_feed($feed['meta']['linkTo']);
		$existing_emails = GF_Civi_Contact_Repository::get()->get_contact_emails($contact_id);
		$match_id = $this->get_update_match_id($existing_emails, $feed, $entry);
		if ($match_id)
		{
			$processed_fields['id'] = $match_id;
		}

		$processed_fields['contact_id'] = $contact_id;
		$processed_fields['location_type_id'] = $feed['meta']['locationTypeId'];
		$processed_fields['email'] = $entry[$feed['meta']['email']];

		$tickboxes = [
			'is_primary' => 'isPrimary',
			'is_billing' => 'isBilling',
			'on_hold' => 'isHold',
			'is_bulkmail' => 'isBulk'
		];
		foreach ($tickboxes as $civi => $tickbox) {;
			if ($feed['meta'][$tickbox]) {
				$processed_fields[$civi] = true;
			}
		}

		$creation_mode = $processed_fields['id'] === 0 ? 'create' : 'update';

		try {
			$created = \Civi\Api4\Email::save(FALSE)
				->setRecords([
					$processed_fields
				])
				->execute();

			GF_Civi_Created_Log::get()->record_success($created->first()['id'], $contact_id, [
				'entity' => 'Phone',
				'mode' => $creation_mode,
				'feed' => $feed['id']
			]);
		} catch ( CiviCRM_API3_Exception $e ) {
			GF_Civi_Created_Log::get()->record_failure([
				'entity' => 'Phone',
				'mode' => $creation_mode,
				'feed' => $feed['id'],
				'error' => $e->getMessage()
			]);
		}
	}

	private function get_update_match_id($existing_emails, $feed, $entry)
	{
		$match_fields = [
			'email' => 'email',
			'location_type_id' => 'locationTypeId',
			'is_primary' => 'isPrimary',
			'is_billing' => 'isBilling',
			'on_hold' => 'isHold',
			'is_bulkmail' => 'isBulk'
		];
		$to_match = [];
		foreach ($match_fields as $civifield => $field)
		{
			if ($feed['meta'][$field . 'Match'])
			{
				$to_match[$civifield] = $entry[$feed['meta'][$field]];
			}
		}

		$match = wp_list_filter($existing_emails, $to_match);
		if (isset($match[0])) {
			return $match[0]['id'];
		}
		return null;
	}

	/**
	 * Dedupe CiviCRM Contact.
	 *
	 * @param array $contact Contact data
	 * @param string $contact_type Contact type
	 * @param int $dedupe_rule_id Dedupe Rule ID
	 * @return int $contact_id The contact id
	 */
	public function civi_contact_dedupe( $contact, $contact_type, $dedupe_rule_id ) {
		// Dupes params
		$dedupeParams = CRM_Dedupe_Finder::formatParams( $contact, $contact_type );
		$dedupeParams['check_permission'] = FALSE;

		// Check dupes
		$cids = CRM_Dedupe_Finder::dupesByParams( $dedupeParams, $contact_type, NULL, [], $dedupe_rule_id );
		$cids = array_reverse( $cids );

		return $cids ? array_pop( $cids ) : 0;
	}


	// # IMPORT / EXPORT -----------------------------------------------------------------------------------------------

	/**
	 * Export Advanced Post Creation Add-On feeds when exporting form.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param array $form The current Form object being exported.
	 *
	 * @uses   GFAddOn::get_slug()
	 * @uses   GFFeedAddOn::get_feeds()
	 *
	 * @return array
	 */
	public function export_feeds_with_form( $form ) {

		// Get feeds for form.
		$feeds = $this->get_feeds( $form['id'] );

		// If form does not have a feeds property, create it.
		if ( ! isset( $form['feeds'] ) ) {
			$form['feeds'] = array();
		}

		// Add feeds to form object.
		$form['feeds'][ $this->get_slug() ] = $feeds;

		return $form;

	}

	/**
	 * Import Advanced Post Creation Add-On feeds when importing form.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param array $forms Imported Form objects.
	 *
	 * @uses   GFAPI::add_feed()
	 * @uses   GFAPI::get_form()
	 * @uses   GFAPI::update_form()
	 */
	public function import_feeds_with_form( $forms ) {

		// Loop through each form being imported.
		foreach ( $forms as $import_form ) {

			// Ensure the imported form is the latest. Compensates for a bug in Gravity Forms < 2.1.1.13
			$form = GFAPI::get_form( $import_form['id'] );

			// If the form does not have any Advanced Post Creation Add-On feeds, skip.
			if ( ! rgars( $form, 'feeds/' . $this->get_slug() ) ) {
				continue;
			}

			// Loop through feeds.
			foreach ( rgars( $form, 'feeds/' . $this->get_slug() ) as $feed ) {

				// Import feed.
				GFAPI::add_feed( $form['id'], $feed['meta'], $this->get_slug() );

			}

			// Remove feeds from Form object.
			unset( $form['feeds'][ $this->get_slug() ] );

			// Remove feeds property if empty.
			if ( empty( $form['feeds'] ) ) {
				unset( $form['feeds'] );
			}

			// Update form.
			GFAPI::update_form( $form );

		}

	}

}