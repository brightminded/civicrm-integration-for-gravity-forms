;(function(jQuery) {
	"use strict";

	function ContactSelect()
	{
		var global = {
			notSpamName: false,
			notSpamValue: false,
			eventNamespace: '.antispam',
			inputSelector: '[name=ishuman]'
		};

		var fn = {
			init: function()
			{
				jQuery('.gfcivi-crmcontacts-select').selectWoo({
                    ajax: {
                        url: ajaxurl,
                        dataType: 'json',
                        type: 'post',
                        delay: 250,
                        dataType: 'json',
                        data: function ( params ) {
                            return {
                                search: params.term,
                                action: 'civicrm_get_contacts',
                                nonce: gfcivi_contact_select_strings.nonce,
                                form_id: gfcivi_contact_select_strings.form_id
                            };
                        },
                        processResults: function( data ) {
                            var options = [];
                            if ( data ) {
                                jQuery.each( data, function( index, contact ) {
                                    options.push( { id: contact['id'], text: contact['text'] } );
                                });
                            }
                            return {
                                results: options
                            };
                        },
                        cache: true
                    },
                    minimumInputLength: 3,
                    allowClear: true,
                    placeholder: 'Search Contacts or link to a Contact processor',
                });
			},
		}

		jQuery(fn.init);
	}

	new ContactSelect();
})(jQuery);