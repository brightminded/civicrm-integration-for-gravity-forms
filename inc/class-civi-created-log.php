<?php
if( !defined( 'ABSPATH' ) ) { exit; }

/**
 * Holds data about entities created, for sync to entity meta
 */
class GF_Civi_Created_Log
{

    public $success = [];
	public $failure = [];

    private static $instance = null;

	/**
	 * Singleton getter
	 *
	 * @return GF_Civi_Created_Log
	 */
	public static function get()
	{
		if ( null === self::$instance )
		{
			self::$instance = new self();
		}

		return self::$instance;
	}

    public function record_success(int $created_id, $parent, $data)
    {
		$data['created_id'] = $created_id;
		if ($parent && isset($this->success[$parent])) {
			$this->success[$parent]['child_records'][$created_id] = $data;
		} else {
			$this->success[$created_id] = $data;
			$this->success[$created_id]['child_records'] = [];
		}
    }

    public function record_failure($data)
    {
        $this->failure[] = $data;
    }
}