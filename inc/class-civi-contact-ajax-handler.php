<?php
if( !defined( 'ABSPATH' ) ) { exit; }

/**
 * Contact lookup
 */
class GF_Civi_Contact_Ajax_Handler
{
    public function __construct()
    {
        add_action( 'wp_ajax_civicrm_get_contacts', [ $this, 'get_civicrm_contacts' ] );
    }

    public function get_civicrm_contacts()
    {
		if (isset($_POST['search'])) {
            $search_term = sanitize_text_field( $_POST['search'] );
        }
		if (isset($_POST['contact_id'])) {
            $contact_id = intval( $_POST['contact_id'] );
        }
        $form_id = false;
        if (isset($_POST['form_id'])) {
            $form_id = intval( $_POST['form_id'] );
        }

		if (!wp_verify_nonce($_POST['nonce'],'admin_get_civi_contact')) {
            return;
        }

		$params = [];

		if (isset($contact_id)) {
            $params['contact_id'] = $contact_id;
        }
			
		// sort name
		if (isset($search_term)) {
            $params['sort_name'] = $search_term;
        }
			

		$contacts = $this->get_contacts($params);
        $feeds = $this->get_contact_feeds($form_id);
        $result = array_merge($feeds, $contacts);

		echo json_encode( $result );
		die;
    }

    public function get_contacts($search) {

		$params = [
			'sequential' => 1,
			'return' => [ 'sort_name', 'email' ],
			'is_deleted' => 0,
			'is_deceased' => 0,
		];

		$params = array_merge( $params, $search );

		$contacts = civicrm_api3( 'Contact', 'get', $params );

		foreach ( $contacts['values'] as $key => $contact ) {
			$sort_name = $contact['sort_name'] . ' :: ' . $contact['email'];
			$result[] = [ 'id' => $contact['id'], 'text' => $sort_name ];
		}

		return $result;
	}

    private function get_contact_feeds($form_id)
	{
		$feeds = GFAPI::get_feeds([], $form_id, 'civicrm_contact', true);
		$return_arr = [];
		foreach ($feeds as $feed) {
			$return_arr[] = [
				'text' => $feed['meta']['feedName'],
				'id' => 'feed_' . $feed['id']
			];
		}
		return $return_arr;
	}

    public static function get_select2_choice($choice, $feed)
    {
        if (isset($feed['meta'][$choice]) && !empty($feed['meta'][$choice])) {
			if (substr($feed['meta'][$choice], 0, 5) === "feed_")
			{
				$label = GFAPI::get_feed(substr($feed['meta'][$choice], 5))['meta']['feedName'];
			}
			else
			{
				$contact = civicrm_api3('Contact', 'get', [
					'id' => $feed['meta'][$choice],
					'sequential' => true
				]);
				$label = $contact['values'][0]['sort_name'] . ' :: ' . $contact['values'][0]['email'];
			}

			return [[
				'label' => $label,
				'value' => $feed['meta'][$choice]
			]];
		}
    }
}
new GF_Civi_Contact_Ajax_Handler();