<?php
if( !defined( 'ABSPATH' ) ) { exit; }

/**
 * Holds shared store of contacts created by feeds
 */
class GF_Civi_Contact_Repository
{

    private $contact_map = [];
    private $contact_emails = [];

    private static $instance = null;

	/**
	 * Singleton getter
	 *
	 * @return GF_Civi_Contact_Repository
	 */
	public static function get()
	{
		if ( null === self::$instance )
		{
			self::$instance = new self();
		}

		return self::$instance;
	}

    public function record(int $contact_id, int $feed_id)
    {
        $this->contact_map[$feed_id] = $contact_id;
    }

    public function get_by_feed(int $feed_id)
    {
        return $this->contact_map[$feed_id] ?? false;
    }

	public function get_contact_emails(int $contact_id)
	{
		if (!isset($this->contact_emails[$contact_id]))
		{
			$this->contact_emails[$contact_id] = \Civi\Api4\Email::get()
				->addWhere('contact_id', '=', 2)
				->execute()
				->jsonSerialize();
		}

		return $this->contact_emails[$contact_id];
	}
}