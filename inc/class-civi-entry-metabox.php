<?php
if( !defined( 'ABSPATH' ) ) { exit; }

/**
 * Displays data about created entities on entry form
 */
class GF_Civi_Entry_Metabox
{
    public function __construct()
    {
        add_filter('gform_entry_detail_meta_boxes', array($this, 'register_meta_box'), 10, 3);
    }

    public function register_meta_box($meta_boxes, $entry, $form)
    {
        $meta_boxes['civicrm'] = array(
            'title'    => 'CiviCRM integration',
            'callback' => array($this, 'metabox_callback'),
            'context'  => 'normal'
        );
        return $meta_boxes;
    }

    public function metabox_callback($args)
    {
        $entry = $args['entry'];

        $success = json_decode(gform_get_meta($entry['id'], 'civi_successful_entities'));
        $failures = json_decode(gform_get_meta($entry['id'], 'civi_failed_entities'));

        if (empty($success) && empty($failures))
        {
            echo "<strong>No CiviCRM entities touched as a result of this entry</strong>";
        }

        if ($success)
        {
            echo "<h2>Successful entities</h2>";
            $this->output_success_table($success);
        }

        if (!empty($success) && !empty($failures))
        {
            echo '<hr>';
        }

        if ($failures)
        {
            echo "<h2>Failed entities</h2>";
        }
    }

    private function output_success_table($success)
    {
        echo '<table class="entry-details-table ">';
        echo '<tr>';
        echo '<td class="entry-view-field-name">';
        echo 'Entity';
        echo '</td>';
        echo '<td class="entry-view-field-name">';
        echo 'Mode';
        echo '</td>';
        echo '</tr>';
        foreach ($success as $row)
        {
            $entity_link = esc_url($this->get_entity_link($row));
            echo '<tr>';
            echo '<td class="entry-view-field-value">';
            echo "<a href='$entity_link'>";
            echo esc_html($row->entity);
            echo '</a>';
            echo '</td>';
            echo '<td class="entry-view-field-value">';
            echo esc_html($row->mode);
            echo '</td>';
            echo '</tr>';
        }
        echo '</table>';
    }

    private function output_failure_table($failure)
    {
        echo '<table class="entry-details-table ">';
        echo '<tr>';
        echo '<td class="entry-view-field-name">';
        echo 'Entity';
        echo '</td>';
        echo '<td class="entry-view-field-name">';
        echo 'Mode';
        echo '</td>';
        echo '</tr>';
        foreach ($failure as $row)
        {
            echo '<tr>';
            echo '<td class="entry-view-field-value">';
            echo esc_html($row->entity);
            echo '</td>';
            echo '<td class="entry-view-field-value">';
            echo esc_html($row->mode);
            echo '</td>';
            echo '</tr>';
        }
        echo '</table>';
    }

    private function get_entity_link($row)
    {
        switch (strtolower($row->entity)) {
            case 'contact':
                return CRM_Utils_System::url('civicrm/contact/view', ['cid' => $row->created_id]);
            break;
        }
    }
}
new GF_Civi_Entry_Metabox();